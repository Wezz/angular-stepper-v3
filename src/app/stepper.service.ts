import { Injectable } from '@angular/core';
import { StepItem } from './stepper/step-items';
import { Step1Component } from './step1/step1.component';
import { Substep1Component } from './substep1/substep1.component';
import { Step2Component } from './step2/step2.component';


@Injectable({
  providedIn: 'root'
})
export class StepperService {

  getSteps() {
    return [
      new StepItem(Step1Component,
      { uniqueId: 'step1main',
        order: 1,
        mainStep: true,
        stepName: 'Step 1',
        buttonText: 'Next to Sub step 1',
        active: false,
        disabled: false,
        mainDropdown: true
        
      }),
      new StepItem(Substep1Component,
        { subStepLevel1: true,
          mainStepDisabled: false,
          MainstepName: 'Step 1',
          stepName: 'Sub step 1',
          buttonText: 'Next to step Sub sub step 1',
          active: false,
          disabled: false,
          dropdown: true
      }),
      new StepItem(Substep1Component,
      { subStepLevel2: true,
        mainStepDisabled: false,
        MainstepName: 'Step 1',
        stepName: 'Sub sub step 1',
        buttonText: 'Next to step 2',
        active: false,
        disabled: false,
        dropdown: true
      }),
      new StepItem(Step2Component,
      { uniqueId: 'step2main',
        order: 2,
        mainStep: true,
        stepName: 'Step 2',
        buttonText: 'Next to Sub step 2',
        active: false,
        disabled: false,
        mainDropdown: true
        
      }),
      new StepItem(Substep1Component,
        { subStepLevel1: true,
          mainStepDisabled: false,
          MainstepName: 'Step 2',
          stepName: 'Sub step 2',
          buttonText: 'Next to step Sub sub step 2',
          active: false,
          disabled: false,
          dropdown: false
      }),
      new StepItem(Substep1Component,
      { subStepLevel2: true,
        mainStepDisabled: false,
        MainstepName: 'Step 2',
        stepName: 'Sub sub step 2',
        buttonText: 'Next to step 3',
        active: false,
        disabled: false,
        dropdown: false
      }),
      new StepItem(Step1Component,
      { uniqueId: 'step3main',
        order: 3,
        mainStep: true,
        stepName: 'Step 3',
        buttonText: 'Back to step 1',
        active: false,
        disabled: true,
        mainDropdown: true
        
      }),
    ];
  }
}
