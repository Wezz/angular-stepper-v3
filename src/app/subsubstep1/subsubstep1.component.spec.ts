import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Subsubstep1Component } from './subsubstep1.component';

describe('Subsubstep1Component', () => {
  let component: Subsubstep1Component;
  let fixture: ComponentFixture<Subsubstep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Subsubstep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Subsubstep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
