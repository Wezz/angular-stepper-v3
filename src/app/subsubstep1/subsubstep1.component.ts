import { Component, Input } from '@angular/core';
import { StepComponent } from './../stepper/step.component';

@Component({
  selector: 'app-subsubstep1',
  templateUrl: './subsubstep1.component.html',
  styleUrls: ['./subsubstep1.component.scss']
})
export class Subsubstep1Component implements StepComponent {

  @Input() data: any;

  constructor() { }

}
