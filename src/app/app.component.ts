
import { Component, OnInit, Input } from '@angular/core';
import { StepItem } from './stepper/step-items';
import { StepperService } from './stepper.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  steps: StepItem[];


  constructor(private stepperService: StepperService) { }

  ngOnInit() {
    this.steps = this.stepperService.getSteps();
  }
}
