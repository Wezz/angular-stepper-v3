import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StepperComponent } from './stepper/stepper.component';
import { InputModule } from 'nedbank-design';
import { HyperlinkModule } from 'nedbank-design';
import { ButtonModule } from 'nedbank-design';
import { ModalModule } from 'nedbank-design';

import { Step1Component } from './../app/step1/step1.component';
import { Step2Component } from './../app/step2/step2.component';
import { StepDirective } from './stepper/stepper.directive';
import { Substep1Component } from './substep1/substep1.component';
import { Subsubstep1Component } from './subsubstep1/subsubstep1.component';


@NgModule({
  declarations: [
    AppComponent,
    StepperComponent,
    StepDirective,
    Step1Component,
    Step2Component,
    Substep1Component,
    Subsubstep1Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    InputModule,
    HyperlinkModule,
    ButtonModule,
    ModalModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ Step1Component, Step2Component],
})
export class AppModule { }
