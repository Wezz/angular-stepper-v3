import { Component, Input } from '@angular/core';
import { StepComponent } from './../stepper/step.component';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class Step1Component implements StepComponent {

  @Input() data: any;

  constructor() { }

}
