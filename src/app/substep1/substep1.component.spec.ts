import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Substep1Component } from './substep1.component';

describe('Substep1Component', () => {
  let component: Substep1Component;
  let fixture: ComponentFixture<Substep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Substep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Substep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
