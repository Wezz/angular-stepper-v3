
import { Component, Input, OnInit, ViewChild, ComponentFactoryResolver, Output, EventEmitter, HostListener} from '@angular/core';
import { StepDirective } from './stepper.directive';
import { StepItem } from './step-items';
import { StepComponent } from './step.component';

@Component({
	selector: 'ui-stepper',
	templateUrl: './stepper.component.html',
	styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {

	@Input() steps: StepItem[];
	@Input() data: any;

	stepButtonText: any;
	currentStepIndex = -1;
	allSteps: any = [];
	mainSteps: any = [];
	mainStepNames: any = [];
	disabledMainStep: any;
	substepMainDisabled: any;
	totalMainSteps: number;
	stepId = 1;
	stepUid;
	currentMainStep = 0;
	active: boolean;
	mobileNavOpen = false;

	// Save and Cancel modal values
	Save: boolean = false;
	Cancel: boolean = false;

	// Save and Cancel event emitters (From within modal component)
	@Output() getSave = new EventEmitter();
	@Output() getCancel = new EventEmitter();

	@ViewChild(StepDirective, {static: true}) stepHost: StepDirective;

	constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

	//////////////////////
	// Click/ Init functions
	/////////////////////

	ngOnInit() {

		// Create allSteps array
		let i;
		for (i = 0; i < this.steps.length; i++) {
			this.allSteps.push(this.steps[i].data);

			// Create mainSteps array from allSteps
			if (this.allSteps[i].mainStep){
				this.mainSteps.push(this.allSteps[i].mainStep);
				this.totalMainSteps = this.mainSteps.length;
			}
		}

		// Run load component on button click
		this.loadComponent();
	
	}

	@HostListener('window:resize', ['$event'])
		onResize(event) {
			this.checkWindow();
		}


	checkWindow(){
		var mq = window.matchMedia( "(max-width: 768px)" );
		if (mq.matches) {
			setTimeout(() => {
				// Get width and height of the window excluding scrollbars
				const windowWidth = document.documentElement.clientWidth;
				const arrows = document.querySelectorAll(".dropdown-arrow");
				[].forEach.call(arrows, (el) => {
					el.style.left = windowWidth - 129 + 'px';
				});	
			}, 100);
		}
		else {
			setTimeout(() => {
				const arrows = document.querySelectorAll(".dropdown-arrow");
				[].forEach.call(arrows, (el) => {
					el.style.left = 123 +  'px';
				});	
			}, 100);
		}
	}


	// Get progess bar height
	getProgress(){
		setTimeout(() => {
			const progress = document.querySelector('.progress-bar-side .progress') as HTMLElement;
			const sidebarElement = document.querySelector('.sidebar') as HTMLElement;
			const ActiveClass = document.querySelector('.activeClass') as HTMLElement;
			const sidebartop = sidebarElement.getBoundingClientRect().top;
			const currentLinktop = ActiveClass.getBoundingClientRect().top;
			const currentLinkHeight = ActiveClass.getBoundingClientRect().height;
			const difference = currentLinktop - sidebartop;

			// console.log(difference);

			//mobile
			const progressMobile = document.querySelector('.progress-bar-top .progress') as HTMLElement;
			const mainStepsCount = document.querySelectorAll('.main-level').length;

			console.log('links height' + currentLinkHeight);
			console.log('links top' + currentLinktop);
			console.log('sidbar top' + sidebartop);


			//Apply height to side progress bar
			progress.style.height = difference + (currentLinkHeight / 2)  +  'px';


			var mq = window.matchMedia( "(max-width: 768px)" );
			if (mq.matches) {
				progress.style.height = difference + (currentLinkHeight / 2)  - 20 +   'px';
			}

			const stepLink = document.querySelectorAll('.sidebar-link');

			[].forEach.call(stepLink, (el) => {

				if(el.classList.contains("main-level")){
					if(el.querySelector("span").classList.contains("activeClass")){
						const currentStep = el.getAttribute("value");
						progressMobile.style.width =  currentStep / mainStepsCount  * 100 + '%';
					}			
				}		
			});

		}, 100);
	}

	// On button click to run loadComponent
	getSteps() {
		this.loadComponent();
	}

	// On link click and load to get current index and update counter
	getStepsSideNav(index, event) {
		const stepItem = this.steps[index];
		this.stepButtonText = stepItem.data.buttonText;

		this.getProgress();

		if (stepItem.data.mainStep) {

			//Reset dropdown arrows
			const reset =  document.querySelectorAll('.nlsg-icon');
			[].forEach.call(reset, (node, i) => {
				node.classList.add('nlsg-icon-arrow-down');
				node.classList.remove('nlsg-icon-arrow-up');
			});

			this.stepId = stepItem.data.order;
			this.stepUid = stepItem.data.uniqueId;

			const currentArrow =  document.querySelector('#'+this.stepUid);
			currentArrow.querySelector('.nlsg-icon').classList.remove('nlsg-icon-arrow-down');
			currentArrow.querySelector('.nlsg-icon').classList.add('nlsg-icon-arrow-up');

			//Reset dropdown arrows end
			this.dropdownCheck(index);

			if (this.stepId <= this.totalMainSteps ) {
				this.currentMainStep = this.stepId;
				this.currentStepIndex = index;
			}
		}
		else {

			if (this.stepId <= this.totalMainSteps ) {
				this.currentMainStep = this.stepId;
				this.currentStepIndex = index;
			}

			this.currentStepIndex = index;
		}
		this.componentFactory(stepItem);
	}

	// Launch modals
	launchModal(event) {

		// Save or Cancel link pressed?
		const currentModal = event.target.parentElement.parentElement.getAttribute("id");

		// Save link pressed
		if(currentModal === 'save'){
			// Emit true using getSave emitter
			this.getSave.emit(true);

			// Change current stepper save modal value to true and canel to false
			this.Save = true;
			this.Cancel = false;
		}

		else {
			// Emit true using getCancel emitter
			this.getCancel.emit(true);

			 // Change current stepper cancel modal value to true and save to false
			this.Save = false;
			this.Cancel = true;
		}
	}

	// Open/close mobile menu
	mobileNav(){

		this.checkWindow();

		if(this.mobileNavOpen == false){
			this.mobileNavOpen = true;
		}
		else {
			this.mobileNavOpen = false;
		}
	}

	//////////////////////
	// Internal functions
	/////////////////////

	dropdownCheck(index){ 
		if (this.allSteps[index]){
			let i;
			for (i = 0; i < this.steps.length; i++) {

				if (this.allSteps[index].stepName === this.allSteps[i].MainstepName){
					this.allSteps[i].dropdown = true;
					this.allSteps[index].mainDropdown = true;

				}
				else {
					this.allSteps[i].dropdown = false;
				}
			}
		}
	}

	// Set active on current item and run component factory to create instances
	componentFactory(stepItem) {
		this.resetActive();

		if (stepItem){
			stepItem.data.active = true;
		}

		const componentFactory = this.componentFactoryResolver.resolveComponentFactory(stepItem.component);
		const viewContainerRef = this.stepHost.viewContainerRef;
		viewContainerRef.clear();
		const componentRef = viewContainerRef.createComponent(componentFactory);

		(<StepComponent>componentRef.instance).data = stepItem.data;
	}

	// Get current index and update counter
	loadComponent() {
		this.currentStepIndex = (this.currentStepIndex + 1) % this.steps.length;
		const stepItem = this.steps[this.currentStepIndex];

		this.stepButtonText = stepItem.data.buttonText;

		if (stepItem.data.mainStep) {
			this.stepId = stepItem.data.order;

			this.dropdownCheck(this.currentStepIndex );

			if (this.stepId <= this.totalMainSteps ) {
				this.currentMainStep = this.stepId;

			}
			else {
					this.currentMainStep = 1;
			}
		}

		this.getProgress();
		this.componentFactory(stepItem);
	}

	// Reset active data to false
	resetActive(){
		let i;
		for (i = 0; i < this.steps.length; i++) {
			this.steps[i].data.active = false;
		}
	}  
}
